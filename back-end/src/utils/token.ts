import jwt from 'jsonwebtoken'
import { jwtSecret } from '../config/auth'
export const createToken = (payLoad: any) => {
	return jwt.sign(payLoad, jwtSecret, { expiresIn: '6h' })
}
