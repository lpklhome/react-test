import seq from './seq'
import './models/index'
// 测试连接
	; (async () => {
		try {
			await seq.authenticate()
			console.log('我连接上了')
		} catch (e) {
			console.log(e)
		}
	})()
	// 同步更新model
	; (async () => { // model配置修改后 执行sync进行同步更新
		await seq.sync({ alter: true })
		console.log('sync ok')
	})()
	