import Koa from 'koa'
import cors from '@koa/cors'
import logger from 'koa-logger'
import bodyParser from 'koa-bodyparser'
import jwt from 'koa-jwt'

import authRoutes from './routes/auth'
import { jwtSecret } from './config/auth'

// Koa应用实例
const app = new Koa()
// middlewares 中间键
app.use(cors()) // 支持跨域
app.use(bodyParser({ // 解析请求体
	enableTypes: ['json', 'form', 'text']
}))
app.use(logger())

// routes
// 用户验证路由（登陆 注册）
app.use(authRoutes.routes()).use(authRoutes.allowedMethods())
// 自定义401错误
app.use((ctx, next) => {
	return next().catch(err => {
		if (err.status == 401) {
			ctx.status = 401
			ctx.body = {
				code: 401,
				error: '未登录 token失效'
			}
		} else {
			ctx.throw(err)
		}
	})
})
// 定义白名单
app.use(jwt({ secret: jwtSecret }).unless({
	path: ['/api/auth/login', '/api/auth/register']
}))
// listen
const port = process.env.PORT || '3003'
app.listen(port, () => {
	console.log(`我在监听${port}`)
})
app.on('error', (err) => {
	console.log('server error', err)
})
