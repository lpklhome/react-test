import { RegisterModel } from "../db/models/user";
import { createUser, getUserInfo } from '../services/auth'
import { SuccessResponse, ErrorResponse } from "../utils/Response";
import errorInfo from "../constants/errorInfo";
import { createMd5 } from "../utils/createMD5";
import { createToken } from "../utils/token";

const {
	registerUserNameExistInfo,
	registerFailInfo,
	loginFailInfo
} = errorInfo
// 调用数据库存model的管理方法 将操作独立出来
// 注册
export const registerController = async (params: RegisterModel) => {
	const { username, password } = params
	// 注册之前先查看用户是否存在
	const userInfo = await getUserInfo({ username })
	if (userInfo) {
		// 已经注册了
		const { code, message } = registerUserNameExistInfo
		return new ErrorResponse(code, message)
	}
	// 用户不存在的情况
	try {
		await createUser({
			...params,
			password: createMd5(password)
		})
		return new SuccessResponse({})
	} catch (err:any) {
		// 注册失败的情况
		console.log(err.message, err.stack)
		const { code, message } = registerFailInfo
		return new ErrorResponse(code, message)

	}
}
interface LoginModel {
	username: string;
	password: string;
}
/**登录操作 */
export const loginController = async (params: LoginModel) => {
	const { username, password } = params
	// 根据用户名和密码获取用户信息
	const userInfo = await getUserInfo({ username, password })
	if (userInfo) { // 用户存在 返回token
		const { id, username } = userInfo
		const token = createToken({ id, username }) // 根据id和用户名生成token
		return new SuccessResponse({ token })
	}
	// 获取不到用户信息
	const { code, message } = loginFailInfo
	return new ErrorResponse(code, message)
}
